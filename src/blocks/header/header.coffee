#import Modernizr from 'modernizr'
#
#$ ->
#	$block = $('.header')
#	return unless $block.length
#	$toggler = $block.find(".header__toggle")
#	$window = $(window)
#
#	handleHeader = ->
#		scrollTop = $window.scrollTop()
#		threshold = 1
#		unless $block.hasClass('header_simple')
#			if scrollTop >= threshold
#				$block.addClass('header_sticky')
#			else if scrollTop <= threshold and $block.hasClass('header_sticky')
#				$block.removeClass('header_sticky')
#
#	handleHeader()
#	$window.on('scroll resize', handleHeader)
#
#
#
#	hideMenu = ->
#		$block.removeClass "header_active"
#		setTimeout ->
#			$("body").removeClass("menu-open")
#		, 200
#		$toggler.removeClass "cross"
#
#	mobileMenuToggl = (e) ->
#		$target = $(e.target)
#		onMenuClick = $target.closest('.header').length
#		onTooglerClick = $target.closest(".header__toggle").length
#		if $("body").hasClass("menu-open")
#			if onTooglerClick or !onMenuClick
#				hideMenu()
#		else
#			if onTooglerClick
#				$toggler.addClass "cross"
#				setTimeout ->
#					$block.addClass "header_active"
#				, 200
#				$("body").addClass("menu-open")
#
#	if Modernizr?.touchevents
#		$(document).on "touchstart", mobileMenuToggl
#	else
#		$(document).on "click", mobileMenuToggl
#
