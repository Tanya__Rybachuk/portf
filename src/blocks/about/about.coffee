import Parallax from 'parallax-js'

$ ->
	$block = $('.about')
	return unless $block.length

	parallax = new Parallax($block.get(0), {
		relativeInput: true,
		selector: '.layer'
	});