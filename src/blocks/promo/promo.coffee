import Parallax from 'parallax-js'

$ ->
	$block = $('.promo')
	return unless $block.length
	$image = $block.find('.promo__bg')
	$image2 = $block.find('.promo__picture')


	parallax = new Parallax($image.get(0), {
		relativeInput: true,
		selector: '.layer'
	});


	parallax = new Parallax($image2.get(0), {
		relativeInput: true,
		selector: '.layer'
	});


