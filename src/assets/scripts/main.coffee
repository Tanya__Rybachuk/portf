import FastClick from 'fastclick';
import Modernizr from 'modernizr';
#import fancybox from 'fancybox';


$ ->
	unless Modernizr.touchevents
		# replace tel: links to callto:
		$("a[href^='tel:']").each ->
			$(@).attr 'href', $(@).attr('href').replace('tel:', 'callto:')
		window.touchDevice = false
	else
		window.touchDevice = true

	FastClick.attach(document.body)

$('.header__menu-link').click ->
	$('html, body').animate { scrollTop: $($(this).attr('href')).offset().top }, 500
	false

#$().fancybox
#	selector: '[data-fancybox="video"]'
#	buttons: [ 'close' ]
#	arrows: false
#	keyboard: false
#	infobar: false

#
#	$('.fancybox').attr('rel', 'gallery').fancybox helpers: thumbs:
#		width: 40
#		height: 40

#$(document).ready ->
#	$('.fancybox').fancybox()
#	return

